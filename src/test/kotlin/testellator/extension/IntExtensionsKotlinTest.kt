package testellator.extension

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@DisplayName("[ Kotlin ] Extensions to Int")
internal class IntExtensionsKotlinTest {

    @Nested
    @DisplayName("Testing that a flag is in place")
    inner class HasFlagTests {

        @Test
        @DisplayName("For one flag")
        fun testInclusionOfOneFlag() {
            assertTrue(8 hasFlag 8)
            assertFalse(8 hasFlag 1)
            assertFalse(8 hasFlag 2)
            assertFalse(8 hasFlag 4)
        }

        @Test
        @DisplayName("For two flags")
        fun testInclusionOfTwoFlags() {
            assertTrue(10 hasFlag 2)
            assertTrue(10 hasFlag 8)
            assertFalse(10 hasFlag 1)
            assertFalse(10 hasFlag 4)
        }
    }

    @Nested
    @DisplayName("Testing that a flag is NOT in place")
    inner class DoesNotHaveFlagTests {

        @Test
        @DisplayName("For one flag")
        fun testNonInclusionOfOneFlag() {
            assertFalse(8 doesNotHaveFlag 8)
            assertTrue(8 doesNotHaveFlag 1)
            assertTrue(8 doesNotHaveFlag 2)
            assertTrue(8 doesNotHaveFlag 4)
        }

        @Test
        @DisplayName("For two flags")
        fun testNonInclusionOfTwoFlags() {
            assertFalse(10 doesNotHaveFlag 2)
            assertFalse(10 doesNotHaveFlag 8)
            assertTrue(10 doesNotHaveFlag 1)
            assertTrue(10 doesNotHaveFlag 4)
        }
    }
}
