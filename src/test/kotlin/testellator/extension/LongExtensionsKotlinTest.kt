package testellator.extension

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import testellator.KotlinTestConstants
import testellator.KotlinTestConstants.KotlinTestClassDisplayNamePrefix

@DisplayName(KotlinTestClassDisplayNamePrefix + "Long extension functions")
internal class LongExtensionsKotlinTest {

    @Test
    fun testAddSmallUnsignedLongsToMinSignedLong() {
        assertEquals(Long.MIN_VALUE, Long.MIN_VALUE + 0x0000_0000_0000_0000_uL)
        assertEquals(Long.MIN_VALUE + 1L, Long.MIN_VALUE + 0x0000_0000_0000_0001_uL)
        assertEquals(-1L, Long.MIN_VALUE + 0x7FFF_FFFF_FFFF_FFFF_uL)
    }

    @Test
    fun testAddSmallUnsignedLongsToZeroSignedLong() {
        assertEquals(0L, 0L + 0x0000_0000_0000_0000_uL)
        assertEquals(1L, 0L + 0x0000_0000_0000_0001_uL)
        assertEquals(Long.MAX_VALUE, 0L + 0x7FFF_FFFF_FFFF_FFFF_uL)
    }

    @Test
    fun testAddLargeUnsignedLongsToMinSignedLong() {
        assertEquals(0L, Long.MIN_VALUE + 0x8000_0000_0000_0000_uL)
        assertEquals(Long.MAX_VALUE, Long.MIN_VALUE + 0xFFFF_FFFF_FFFF_FFFF_uL)
    }

    @Test
    fun testSubtractSmallUnsignedLongsFromMaxSignedLong() {
        assertEquals(Long.MAX_VALUE, Long.MAX_VALUE -  0x0000_0000_0000_0000_uL)
        assertEquals(Long.MAX_VALUE - 1L, Long.MAX_VALUE - 0x0000_0000_0000_0001_uL)
        assertEquals(1L, Long.MAX_VALUE - 0x7FFF_FFFF_FFFF_FFFE_uL)
    }

    @Test
    fun testSubtractSmallUnsignedLongsFromZeroSignedLong() {
        assertEquals(0L, 0L - 0x0000_0000_0000_0000_uL)
        assertEquals(-1L, 0L - 0x0000_0000_0000_0001_uL)
        assertEquals(Long.MIN_VALUE, 0L - 0x8000_0000_0000_0000_uL)
    }

    @Test
    fun testSubtractLargeUnsignedLongsFromMaxSignedLong() {
        assertEquals(0L, Long.MAX_VALUE - 0x7FFF_FFFF_FFFF_FFFF_uL)
        assertEquals(Long.MIN_VALUE, Long.MAX_VALUE - 0xFFFF_FFFF_FFFF_FFFF_uL)
    }

    @Test
    fun testAdditionSymmetry() {
        assertEquals(Long.MIN_VALUE + ULong.MAX_VALUE, ULong.MAX_VALUE + Long.MIN_VALUE)
        assertEquals(0L + 1uL, 1uL + 0L)
    }
}
