package testellator.extension

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import testellator.KotlinTestConstants.KotlinTestClassDisplayNamePrefix
import testellator.core.generators.countable.numeric.ints.IntSpace

@DisplayName(KotlinTestClassDisplayNamePrefix + "Partial space extension functions")
internal class PartialSpaceExtensionsKotlinTest {

    @Test
    fun testFirstEntryOfFirstRange() {
        assertEquals(67, IntSpace.indexIntoRanges(listOf(67..98, 95..104), 0uL))
    }

    @Test
    fun testLastEntryOfFirstRange() {
        assertEquals(98, IntSpace.indexIntoRanges(listOf(67..98, 95..104), 31uL))
    }

    @Test
    fun testFirstEntryOfSubsequentRange() {
        assertEquals(95, IntSpace.indexIntoRanges(listOf(67..98, 95..104), 32uL))
    }

    @Test
    fun testLastEntryOfSubsequentRange() {
        assertEquals(104, IntSpace.indexIntoRanges(listOf(67..98, 95..104), 41uL))
    }

    @Test
    fun testNoEntries() {
        assertThrows(IndexOutOfBoundsException::class.java) {
            IntSpace.indexIntoRanges(listOf(), 0uL)
        }
    }

    @Test
    fun testBeyondLastEntry() {
        assertThrows(IndexOutOfBoundsException::class.java) {
            IntSpace.indexIntoRanges(listOf(67..98, 95..104), 42uL)
        }
    }
}
