package testellator.core.generators.countable

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import testellator.core.KotlinTestConstants.AFewTimes
import testellator.core.generators.Generator
import testellator.core.spaces.Space

object CountableGeneratingTests {

    fun <T, G : HasAny<T>> testEqualTo(generating: G, equalToOrBeing: (G, T) -> Generator<T>) {
        val anyValueGenerator = generating.any()
        for (i in AFewTimes) {
            val expectedValue = anyValueGenerator.next()
            val generator = equalToOrBeing.invoke(generating, expectedValue)
            for (j in AFewTimes) {
                Assertions.assertEquals(expectedValue, generator.next())
            }
        }
    }

    fun <T : Comparable<T>, G : HasAny<T>, U> testGreaterThan(
        generating: G,
        space: Space<T>,
        greaterThan: (G, T) -> U,
        lessThanOrEqualTo: (U, T) -> Generator<T>
    ) {
        val anyValueGenerator = generating.any()
        for (i in AFewTimes) {
            // TODO: This will fail if we generate a value right at the end of the range
            val exclusiveMinValue = anyValueGenerator.next()
            val inclusiveMaxValue = space.shiftRight(exclusiveMinValue, 2uL)
            val lowerBounded = greaterThan.invoke(generating, exclusiveMinValue)
            val generator = lessThanOrEqualTo.invoke(lowerBounded, inclusiveMaxValue)
            for (j in AFewTimes) {
                val value = generator.next()
                assertTrue(value > exclusiveMinValue)
                assertTrue(value <= inclusiveMaxValue)
            }
        }
    }

    fun <T : Comparable<T>, G : HasAny<T>, U> testGreaterThanOrEqualTo(
        generating: G,
        space: Space<T>,
        greaterThanOrEqualTo: (G, T) -> U,
        lessThan: (U, T) -> Generator<T>
    ) {
        val anyValueGenerator = generating.any()
        for (i in AFewTimes) {
            // TODO: This will fail if we generate a value right at the end of the range
            val inclusiveMinValue = anyValueGenerator.next()
            val exclusiveMaxValue = space.shiftRight(inclusiveMinValue, 2uL)
            val lowerBounded = greaterThanOrEqualTo.invoke(generating, inclusiveMinValue)
            val generator = lessThan.invoke(lowerBounded, exclusiveMaxValue)
            for (j in AFewTimes) {
                val value = generator.next()
                assertTrue(value >= inclusiveMinValue)
                assertTrue(value < exclusiveMaxValue)
            }
        }
    }
}
