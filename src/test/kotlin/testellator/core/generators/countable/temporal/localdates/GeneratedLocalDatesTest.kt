package testellator.core.generators.countable.temporal.localdates

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import testellator.KotlinTestConstants.KotlinTestClassDisplayNamePrefix
import testellator.core.generators.Generated
import testellator.core.generators.countable.temporal.TemporallyGeneratingTests

@DisplayName(KotlinTestClassDisplayNamePrefix + "Generated LocalDates")
internal class GeneratedLocalDatesTest {

    @Test
    @DisplayName("Being a value")
    fun testBeing() {
        TemporallyGeneratingTests.testBeing(generatingObject)
    }

    @Test
    @DisplayName("Being after a value")
    fun testAfter() {
        TemporallyGeneratingTests.testAfter(generatingObject, generatingSpace)
    }

    @Test
    @DisplayName("Being on or after a value")
    fun testOnOrAfter() {
        TemporallyGeneratingTests.testOnOrAfter(generatingObject, generatingSpace)
    }

    private val generatingObject = Generated.LocalDates
    private val generatingSpace = LocalDateSpace
}
