package testellator.core.generators.countable.temporal

import testellator.core.generators.countable.CountableGeneratingTests
import testellator.core.generators.countable.numeric.IsTemporallyGenerating
import testellator.core.spaces.Space

object TemporallyGeneratingTests {

    fun <T> testBeing(generating: IsTemporallyGenerating<T>) {
        CountableGeneratingTests.testEqualTo(generating, IsTemporallyGenerating<T>::being)
    }

    fun <T : Comparable<T>> testAfter(generating: IsTemporallyGenerating<T>, space: Space<T>) {
        CountableGeneratingTests.testGreaterThan(
            generating,
            space,
            IsTemporallyGenerating<T>::after,
            IsTemporallyLowerBounded<T>::andOnOrBefore
        )
    }

    fun <T : Comparable<T>> testOnOrAfter(generating: IsTemporallyGenerating<T>, space: Space<T>) {
        CountableGeneratingTests.testGreaterThanOrEqualTo(
            generating,
            space,
            IsTemporallyGenerating<T>::onOrAfter,
            IsTemporallyLowerBounded<T>::andBefore
        )
    }
}
