package testellator.core.generators.countable.numeric

import org.junit.jupiter.api.assertThrows
import testellator.core.KotlinTestConstants.AFewTimes
import testellator.core.generators.BoundType.*
import testellator.core.generators.countable.CountableGeneratingTests
import testellator.core.generators.countable.CountableGenerator
import testellator.core.generators.exceptions.EqualToIsMoreSuccinctException
import testellator.core.generators.exceptions.LowerBoundIsGreaterThanUpperBoundException
import testellator.core.generators.exceptions.RangeIntersectsWithPreviouslyAddedException
import testellator.core.spaces.Space

object NumericallyGeneratingTests {

    fun <T> testEqualTo(generating: IsNumericallyGenerating<T>) {
        CountableGeneratingTests.testEqualTo(generating, IsNumericallyGenerating<T>::equalTo)
    }

    fun <T : Comparable<T>> testGreaterThan(generating: IsNumericallyGenerating<T>, space: Space<T>) {
        CountableGeneratingTests.testGreaterThan(
            generating,
            space,
            IsNumericallyGenerating<T>::greaterThan,
            IsNumericallyLowerBounded<T>::andLessThanOrEqualTo
        )
    }

    fun <T : Comparable<T>> testGreaterThanOrEqualTo(generating: IsNumericallyGenerating<T>, space: Space<T>) {
        CountableGeneratingTests.testGreaterThanOrEqualTo(
            generating,
            space,
            IsNumericallyGenerating<T>::greaterThanOrEqualTo,
            IsNumericallyLowerBounded<T>::andStrictlyLessThan
        )
    }

    fun <T : Comparable<T>> testAddRangeWhereLowerBoundIsGreaterThanUpperBound(generating: IsNumericallyGenerating<T>) {
        val generator = generating.any() as CountableGenerator<T>
        val bound = generator.next()
        assertThrows<LowerBoundIsGreaterThanUpperBoundException> {
            generator.addRange(bound, Exclusive, bound, Inclusive, false)
        }
    }

    fun <T> testAddRangeWhereEqualToIsMoreSuccinct(generating: IsNumericallyGenerating<T>) {
        val generator = generating.any()
        for (i in AFewTimes) {
            val boundValue = generator.next()
            assertThrows<EqualToIsMoreSuccinctException> {
                generating greaterThanOrEqualTo boundValue andLessThanOrEqualTo boundValue
            }
        }
    }

    fun <T> testAddRangeWhichIntersectsWithPreviouslyAdded(generating: IsNumericallyGenerating<T>) {
        val generator = generating.any()
        for (i in AFewTimes) {
            val value = generator.next()
            assertThrows<RangeIntersectsWithPreviouslyAddedException> {
                generating equalTo value orEqualTo value
            }
        }
    }
}
