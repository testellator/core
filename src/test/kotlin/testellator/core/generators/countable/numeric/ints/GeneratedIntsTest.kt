package testellator.core.generators.countable.numeric.ints

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import testellator.core.generators.Generated
import testellator.core.generators.countable.numeric.NumericallyGeneratingTests

@DisplayName("[ kotlin ] Generated Ints")
internal class GeneratedIntsTest {

    @Test
    @DisplayName("Equal to a value")
    fun testEqualTo() {
        NumericallyGeneratingTests.testEqualTo(generatingObject)
    }

    @Test
    @DisplayName("Greater than a value")
    fun testGreaterThan() {
        NumericallyGeneratingTests.testGreaterThan(generatingObject, generatingSpace)
    }

    @Test
    @DisplayName("Greater than or equal to a value")
    fun testGreaterThanOrEqualTo() {
        NumericallyGeneratingTests.testGreaterThanOrEqualTo(generatingObject, generatingSpace)
    }

    @Test
    @DisplayName("Trying to add a range where the lower bound is greater than the upper bound")
    fun testAddRangeWhereLowerBoundIsGreaterThanUpperBound() {
        NumericallyGeneratingTests.testAddRangeWhereLowerBoundIsGreaterThanUpperBound(generatingObject)
    }

    @Test
    @DisplayName("Trying to add a single value range by inequality when equality would be more succinct")
    fun testAddRangeWhereEqualToIsMoreSuccinct() {
        NumericallyGeneratingTests.testAddRangeWhereEqualToIsMoreSuccinct(generatingObject)
    }

    @Test
    @DisplayName("Trying to add a range that intersects with one added previously")
    fun testAddRangeWhichIntersectsWithPreviouslyAdded() {
        NumericallyGeneratingTests.testAddRangeWhichIntersectsWithPreviouslyAdded(generatingObject)
    }

    private val generatingObject = Generated.Ints
    private val generatingSpace = IntSpace
}
