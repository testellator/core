package testellator.core.generators.countable.numeric.longs

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import testellator.core.KotlinTestConstants.AFewTimes
import kotlin.random.Random

@DisplayName("[ kotlin ] Long Space")
internal class LongSpaceTest {

    @Test
    fun testMeasureOfLengthOne() {
        for (i in AFewTimes) {
            val value = Random.nextLong()
            assertEquals(1uL, LongSpace.measure(value..value))
        }
    }

    @Test
    fun testMeasureOfLengthTwo() {
        for (i in AFewTimes) {
            val min = Random.nextLong(Long.MIN_VALUE, Long.MAX_VALUE)
            val max = min + 1L
            assertEquals(2uL, LongSpace.measure(min..max))
        }
    }

    @Test
    fun testMeasureOfAlmostFullRange() {
        assertEquals(ULong.MAX_VALUE, LongSpace.measure(Long.MIN_VALUE until Long.MAX_VALUE))
    }

    @Test
    fun testMeasureOfFullRange() {
        assertEquals(0uL, LongSpace.measure(LongSpace.fullRange()))
    }
}
