package testellator.core.generators

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import testellator.KotlinTestConstants.KotlinTestClassDisplayNamePrefix
import testellator.core.generators.BoundType.*

@DisplayName(KotlinTestClassDisplayNamePrefix + "Bound types")
internal class BoundTypeTest {

    @Test
    @DisplayName("are correctly specified")
    fun test() {
        assertEquals(2, values().size)

        assertEquals(0uL, Inclusive.offset)
        assertEquals(1uL, Exclusive.offset)

        assertEquals('[', Inclusive.openingCharacter)
        assertEquals('(', Exclusive.openingCharacter)

        assertEquals(']', Inclusive.closingCharacter)
        assertEquals(')', Exclusive.closingCharacter)
    }
}
