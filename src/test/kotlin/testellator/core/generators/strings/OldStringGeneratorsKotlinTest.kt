package testellator.core.generators.strings

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.platform.commons.logging.Logger
import org.junit.platform.commons.logging.LoggerFactory
import testellator.core.KotlinTestConstants.AFewIterations
import testellator.core.generators.Generated.Strings
import testellator.core.generators.strings.NamedCharacterSet.LowerCaseLettersAndNumerals
import kotlin.random.Random.Default.nextInt

@DisplayName("[ Kotlin ] String generators")
class OldStringGeneratorsKotlinTest {

    private val logger: Logger = LoggerFactory.getLogger(OldStringGeneratorsKotlinTest::class.java)

    @Test
    @DisplayName("can be expressed using infix style")
    fun testInfixStyle() {

        val generator = Strings betweenLength 4 and 16 including LowerCaseLettersAndNumerals excluding 'c'

        for (i in 1..AFewIterations) {
            logger.info { "Iteration [${i}] produced String [${generator.next()}]" }
        }
    }

    @Test
    @DisplayName("can be expressed using conventional non-infix style")
    fun testConventionalStyle() {

        val generator = Strings.betweenLength(4).and(16).including(LowerCaseLettersAndNumerals).excluding('c')

        for (i in 1..AFewIterations) {
            logger.info { "Iteration [${i}] produced String [${generator.next()}]" }
        }
    }

    @Test
    @DisplayName("with exact length")
    fun testOfLength() {
        for (lengthIteration in 1..AFewIterations) {
            val length = nextInt(1, 64)
            val generator = Strings ofLength length including LowerCaseLettersAndNumerals

            for (generationForLengthIteration in 1..AFewIterations) {
                assertEquals(length, generator.next().length)
            }
        }
    }
}
