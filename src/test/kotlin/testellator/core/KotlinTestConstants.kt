package testellator.core

object KotlinTestConstants {
    const val AFewIterations = 10
    val AFewTimes = 1..AFewIterations
}
