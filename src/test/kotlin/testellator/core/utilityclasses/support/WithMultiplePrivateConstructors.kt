package testellator.core.utilityclasses.support

class WithMultiplePrivateConstructors private constructor(arg: Boolean) {
    private constructor() : this(false)
}
