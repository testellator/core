package testellator.core.utilityclasses

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import testellator.core.utilityclasses.support.*

@DisplayName("[ Kotlin ] Utility Classes")
internal class UtilityClassesTest {

    @Nested
    @DisplayName("Just one constructor which is private and has no arguments")
    inner class JustOneConstructorWhichIsPrivateAndHasNoArgumentsTest {

        @Test
        @DisplayName("FALSE for single public no-argument constructor")
        fun testSinglePublicNoArgsConstructor() {
            assertFalse(
                UtilityClasses.hasJustOneConstructorWhichIsPrivateAndHasNoArguments(
                    WithSinglePublicNoArgsConstructor::class.java
                )
            )
        }

        @Test
        @DisplayName("FALSE for multiple private constructors")
        fun testMultiplePrivateConstructors() {
            assertFalse(
                UtilityClasses.hasJustOneConstructorWhichIsPrivateAndHasNoArguments(
                    WithMultiplePrivateConstructors::class.java
                )
            )
        }

        @Test
        @DisplayName("FALSE for single private constructor with single argument")
        fun testSinglePrivateConstructorsWithSingleArgument() {
            assertFalse(
                UtilityClasses.hasJustOneConstructorWhichIsPrivateAndHasNoArguments(
                    WithSinglePrivateSingleArgConstructor::class.java
                )
            )
        }

        @Test
        @DisplayName("TRUE for a static object class")
        fun testAnObject() {
            assertTrue(
                UtilityClasses.hasJustOneConstructorWhichIsPrivateAndHasNoArguments(
                    AnObject::class.java
                )
            )
        }

        @Test
        @DisplayName("TRUE for single no-args constructor")
        fun testSinglePrivateNoArgsConstructor() {
            assertTrue(
                UtilityClasses.hasJustOneConstructorWhichIsPrivateAndHasNoArguments(
                    WithSinglePrivateNoArgsConstructor::class.java
                )
            )
        }
    }
}



