package testellator.core.enums

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@DisplayName("[ Kotlin ] Enumeration test utilities")
internal class EnumsKotlinTest {

    @Nested
    @DisplayName("Generating a string that is guaranteed not to be the name of any enum value")
    inner class NotNameOfATests {

        @Test
        @DisplayName("Works for a sample enum")
        fun test() {
            val expectedNonName = Enums.notNameOfA(Enum1::class.java)
            assertEquals("XYYX", expectedNonName)
            assertThrows(IllegalArgumentException::class.java) {
                Enum1.valueOf(expectedNonName)
            }
        }
    }

    @Nested
    @DisplayName("Choosing a character that is different from the n-th character of the name of an Enum value with ordinal n")
    inner class DifferentCharacterFromOrdinalTests {

        @Test
        @DisplayName("When name is longer than N and the character at index N is 'X'")
        fun test_name_longer_than_n_and_character_at_n_is_x() {
            assertEquals('Y', Enums.differentCharacterFromOrdinal(Enum1.DXF))
        }

        @Test
        @DisplayName("When name is longer than N and the character at index N is NOT 'X'")
        fun test_name_longer_than_n_and_character_at_n_is_NOT_x() {
            assertEquals('X', Enums.differentCharacterFromOrdinal(Enum1.ABC))
        }

        @Test
        @DisplayName("When name is of length N and the character at index N is 'X'")
        fun test_name_of_length_n_and_character_at_n_is_x() {
            assertEquals('Y', Enums.differentCharacterFromOrdinal(Enum1.GHX))
        }

        @Test
        @DisplayName("When name is shorter than N")
        fun test_name_shorter_than_n() {
            assertEquals('X', Enums.differentCharacterFromOrdinal(Enum1.JKL))
        }
    }

    private enum class Enum1 {
        ABC,
        DXF,
        GHX,
        JKL
    }
}
