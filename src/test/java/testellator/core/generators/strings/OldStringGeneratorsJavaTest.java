package testellator.core.generators.strings;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import testellator.core.generators.Generated;
import testellator.core.generators.Generator;

import static java.lang.String.format;
import static testellator.core.generators.strings.NamedCharacterSet.LowerCaseLettersAndNumerals;

@DisplayName("[ Java ] String generators")
public class OldStringGeneratorsJavaTest {

    private final Logger logger = LoggerFactory.getLogger(OldStringGeneratorsJavaTest.class);

    @Test
    @DisplayName("can be expressed using conventional non-infix style")
    public void testConventionalStyle() {
        final Generator<String> generator = Generated.Strings().betweenLength(4).and(16)
                .including(LowerCaseLettersAndNumerals)
                .excluding('c');
        for (int i = 1; i <= 10; i++) {
            int finalI = i;
            logger.info(() -> format("Iteration [%d] produced String [%s]", finalI, generator.next()));
        }
    }
}
