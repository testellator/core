package testellator.core.utilityclasses;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static testellator.core.utilityclasses.UtilityClasses.hasJustOneConstructorWhichIsPrivateAndHasNoArguments;

@DisplayName("[ java ] Utility Classes")
public class UtilityClassesJavaTest {

    @Nested
    @DisplayName("Verifying a class has just one constructor which is private and has no arguments")
    class JustOneConstructorWhichIsPrivateAndHasNoArgumentsTests {

        @Test
        @DisplayName("Returns true for a class with just one constructor which is private and has no arguments")
        public void testJustOneConstructorWhichIsPrivateAndHasNoArguments() {
            assertTrue(hasJustOneConstructorWhichIsPrivateAndHasNoArguments(
                    ClassWithJustOneConstructorWhichIsPrivateAndHasNoArguments.class));
        }

        @Test
        @DisplayName("Returns false for a class with just one constructor which is public")
        public void testJustOneConstructorWhichIsPublic() {
            assertFalse(hasJustOneConstructorWhichIsPrivateAndHasNoArguments(
                    ClassWithJustOneConstructorWhichIsPublic.class));
        }

        @Test
        @DisplayName("Returns false for a class with two constructors which are both private")
        public void testTwoConstructorWhichAreBothPrivate() {
            assertFalse(hasJustOneConstructorWhichIsPrivateAndHasNoArguments(
                    ClassWithTwoConstructorsWhichAreBothPrivate.class));
        }
    }


    private static class ClassWithJustOneConstructorWhichIsPrivateAndHasNoArguments {
        private ClassWithJustOneConstructorWhichIsPrivateAndHasNoArguments() {
            // Intentionally left blank
        }
    }

    private static class ClassWithJustOneConstructorWhichIsPublic {
        public ClassWithJustOneConstructorWhichIsPublic() {
            // Intentionally left blank
        }
    }

    private static class ClassWithTwoConstructorsWhichAreBothPrivate {
        private ClassWithTwoConstructorsWhichAreBothPrivate() {
            // Intentionally left blank
        }

        private ClassWithTwoConstructorsWhichAreBothPrivate(final Object object) {
            // Intentionally left blank
        }
    }
}
