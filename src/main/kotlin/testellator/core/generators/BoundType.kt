package testellator.core.generators

enum class BoundType constructor(val offset: ULong, val openingCharacter: Char, val closingCharacter: Char) {
    Inclusive(0uL, '[', ']'),
    Exclusive(1uL, '(', ')')
}
