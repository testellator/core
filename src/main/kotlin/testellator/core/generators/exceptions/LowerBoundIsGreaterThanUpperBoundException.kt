package testellator.core.generators.exceptions

import testellator.core.generators.BoundType

class LowerBoundIsGreaterThanUpperBoundException(
    lowerBound: Any,
    lowerBoundType: BoundType,
    inclusiveLowerBound: Any,
    upperBound: Any,
    upperBoundType: BoundType,
    inclusiveUpperBound: Any
) : IllegalArgumentException(
    "Inclusive lower bound [$inclusiveLowerBound] > inclusive upper bound [$inclusiveUpperBound] derived from ${lowerBoundType.openingCharacter} $lowerBound , $upperBound ${upperBoundType.closingCharacter}"
)
