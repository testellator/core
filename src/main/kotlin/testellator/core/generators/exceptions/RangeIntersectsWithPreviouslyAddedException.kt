package testellator.core.generators.exceptions

class RangeIntersectsWithPreviouslyAddedException(
    range: ClosedRange<*>,
    intersectingRangeStrings: Collection<ClosedRange<*>>
) : IllegalArgumentException(
    "Can not add range $range because it intersects with previously added range${if (intersectingRangeStrings.size > 1) "s" else ""} ${intersectingRangeStrings.joinToString()}"
)
