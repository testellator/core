package testellator.core.generators.strings

class LengthFullyBounded internal constructor(
    private val generator: StringGenerator,
    lowerBound: Int,
    upperBound: Int,
    checkBrevity: Boolean = true
): IsLengthFullyBounded<String> {

    internal constructor(value: Int) : this(StringGenerator(), value)

    internal constructor(generator: StringGenerator, value: Int) : this(generator, value, value, false)

    init {
        generator.addLengthRange(lowerBound, upperBound, checkBrevity)
    }

    override fun excluding(character: Char): IsLengthFullyBounded<String> {
        generator.excludeCharacters(setOf(character))
        return this
    }

    override fun excluding(characterSet: Set<Char>): IsLengthFullyBounded<String> {
        generator.excludeCharacters(characterSet)
        return this
    }

    override fun excluding(namedCharacterSet: NamedCharacterSet): IsLengthFullyBounded<String> {
        generator.excludeCharacters(namedCharacterSet.characters())
        return this
    }

    override fun including(character: Char): IsLengthFullyBounded<String> {
        generator.includeCharacters(setOf(character))
        return this
    }

    override fun including(characterSet: Set<Char>): IsLengthFullyBounded<String> {
        generator.includeCharacters(characterSet)
        return this
    }

    override fun including(namedCharacterSet: NamedCharacterSet): IsLengthFullyBounded<String> {
        generator.includeCharacters(namedCharacterSet.characters())
        return this
    }

    override fun next(): String = generator.next()
}
