package testellator.core.generators.strings

interface CharacterSet {
    fun characters(): Set<Char>
}
