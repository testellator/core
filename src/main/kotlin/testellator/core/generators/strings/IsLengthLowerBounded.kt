package testellator.core.generators.strings

interface IsLengthLowerBounded<T> : HasAnd<T>
