package testellator.core.generators.strings

interface HasIncludingCharacter<T> {
    infix fun including(character: Char): IsLengthFullyBounded<T>
}
