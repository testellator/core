package testellator.core.generators.strings

interface HasExcludingCharacterSet<T> {
    infix fun excluding(characterSet: Set<Char>): IsLengthFullyBounded<T>
}
