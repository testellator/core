package testellator.core.generators.strings

interface HasExcludingCharacter<T> {
    infix fun excluding(character: Char): IsLengthFullyBounded<T>
}
