package testellator.core.generators.strings

interface HasBetweenLength<T> {
    infix fun betweenLength(lowerBound: Int): IsLengthLowerBounded<T>
}
