package testellator.core.generators.strings

internal object NamedCharacterSubsets {
    internal val LowerCaseLetterSet = ('a'..'z').toSet()
    internal val NumeralSet = ('0'..'9').toSet()
    internal val UpperCaseLetterSet = ('A'..'Z').toSet()
}
