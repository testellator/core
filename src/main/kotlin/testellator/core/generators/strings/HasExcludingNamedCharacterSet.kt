package testellator.core.generators.strings

interface HasExcludingNamedCharacterSet<T> {
    infix fun excluding(namedCharacterSet: NamedCharacterSet): IsLengthFullyBounded<T>
}
