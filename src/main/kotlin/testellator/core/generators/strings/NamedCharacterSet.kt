package testellator.core.generators.strings

import testellator.core.generators.strings.NamedCharacterSubsets.LowerCaseLetterSet
import testellator.core.generators.strings.NamedCharacterSubsets.NumeralSet
import testellator.core.generators.strings.NamedCharacterSubsets.UpperCaseLetterSet

/**
 * Frequently used categories of characters such as lower-case letters and whitespace for use in character and string
 * generation
 */
enum class NamedCharacterSet(
    private val characters: Set<Char>
) : CharacterSet {
    LowerCaseLetters(
        LowerCaseLetterSet
    ),
    LowerCaseLettersAndNumerals(
        LowerCaseLetterSet,
        NumeralSet
    ),
    Numerals(
        NumeralSet
    ),
    UpperCaseLetters(
        UpperCaseLetterSet
    ),
    UpperCaseLettersAndNumerals(
        UpperCaseLetterSet,
        NumeralSet
    )
    ;

    constructor(vararg subsets: Set<Char>) : this(subsets.flatMap { it }.toSet())

    override fun characters(): Set<Char> = characters
}
