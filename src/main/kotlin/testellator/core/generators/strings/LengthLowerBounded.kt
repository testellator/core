package testellator.core.generators.strings

class LengthLowerBounded internal constructor(
    private val generator: StringGenerator,
    private val lowerBound: Int
) : IsLengthLowerBounded<String> {

    internal constructor(lowerBound: Int) : this(StringGenerator(), lowerBound)

    override fun and(upperBound: Int): IsLengthFullyBounded<String> = LengthFullyBounded(generator, lowerBound, upperBound)
}
