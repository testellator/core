package testellator.core.generators.strings

class GeneratedStrings : IsStringGenerating<String> {
    override fun betweenLength(lowerBound: Int): IsLengthLowerBounded<String> = LengthLowerBounded(lowerBound)
    override fun ofLength(length: Int): IsLengthFullyBounded<String> = LengthFullyBounded(length)
}
