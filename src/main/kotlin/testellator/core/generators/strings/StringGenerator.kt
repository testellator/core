package testellator.core.generators.strings

import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.Generator
import testellator.core.generators.countable.numeric.ints.IntSpace
import testellator.core.generators.exceptions.EqualToIsMoreSuccinctException
import testellator.core.generators.exceptions.LowerBoundIsGreaterThanUpperBoundException
import testellator.core.generators.exceptions.RangeIntersectsWithPreviouslyAddedException
import testellator.extension.indexIntoRanges
import java.util.*
import kotlin.collections.HashSet
import kotlin.random.asKotlinRandom
import kotlin.random.nextInt
import kotlin.random.nextULong

class StringGenerator : Generator<String> {

    private val random = Random().asKotlinRandom()
    private val allowedCharacters: MutableSet<Char> = HashSet()
    private val lengthRanges: MutableList<ClosedRange<Int>> = LinkedList()
    private var lengthCount: ULong = 0uL

    override fun next(): String {
        val lengthIndex = random.nextULong(0uL, lengthCount)
        val length = IntSpace.indexIntoRanges(lengthRanges, lengthIndex)
        val allowedCharacterList = allowedCharacters.toList()
        // TODO: UInt to Int
        return (0 until length.toInt()).map {
            allowedCharacterList[random.nextInt(allowedCharacters.indices)]
        }.joinToString("")
    }

    internal fun includeCharacters(characters: Set<Char>) {
        allowedCharacters.addAll(characters);
    }

    internal fun excludeCharacters(characters: Set<Char>) {
        allowedCharacters.removeAll(characters);
    }

    internal fun addLengthRange(lowerBound: Int, upperBound: Int, checkBrevity: Boolean) {
        if (lowerBound > upperBound)
            throw LowerBoundIsGreaterThanUpperBoundException(
                lowerBound, Inclusive, lowerBound,
                upperBound, Inclusive, upperBound
            )

        // TODO: "EqualsTo" needs to be made more generic
        if (checkBrevity && lowerBound == upperBound)
            throw EqualToIsMoreSuccinctException(
                lowerBound, Inclusive, lowerBound,
                upperBound, Inclusive, upperBound
            )

        val lengthRange = lowerBound..upperBound
        val intersectingLengthRanges = intersectingRanges(lengthRange)
        if (intersectingLengthRanges.isNotEmpty())
            throw RangeIntersectsWithPreviouslyAddedException(lengthRange, intersectingLengthRanges)

        addLengthRange(lengthRange)
    }

    private fun intersectingRanges(lengthRange: ClosedRange<Int>): Collection<ClosedRange<Int>> =
        lengthRanges.filter { IntSpace.intersect(it, lengthRange) }

    private fun addLengthRange(lengthRange: ClosedRange<Int>) {
        lengthRanges.add(lengthRange)
        lengthCount += IntSpace.measure(lengthRange)
    }
}
