package testellator.core.generators.strings

interface HasIncludingNamedCharacterSet<T> {
    infix fun including(namedCharacterSet: NamedCharacterSet): IsLengthFullyBounded<T>
}
