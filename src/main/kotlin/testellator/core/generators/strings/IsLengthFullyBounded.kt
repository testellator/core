package testellator.core.generators.strings

import testellator.core.generators.Generator

interface IsLengthFullyBounded<T> :
    Generator<T>,
    HasExcludingCharacter<T>,
    HasExcludingCharacterSet<T>,
    HasExcludingNamedCharacterSet<T>,
    HasIncludingCharacter<T>,
    HasIncludingCharacterSet<T>,
    HasIncludingNamedCharacterSet<T>
