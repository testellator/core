package testellator.core.generators.strings

interface IsStringGenerating<T> :
    HasBetweenLength<T>,
    HasOfLength<T>
