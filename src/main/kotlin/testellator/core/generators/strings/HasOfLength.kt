package testellator.core.generators.strings

interface HasOfLength<T> {
    infix fun ofLength(length: Int): IsLengthFullyBounded<T>
}
