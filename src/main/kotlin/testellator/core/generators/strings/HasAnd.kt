package testellator.core.generators.strings

interface HasAnd<T> {
    infix fun and(upperBound: Int): IsLengthFullyBounded<T>
}
