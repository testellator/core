package testellator.core.generators.strings

interface HasIncludingCharacterSet<T> {
    infix fun including(characterSet: Set<Char>): IsLengthFullyBounded<T>
}
