package testellator.core.generators.countable.temporal

interface HasBeing<T> {
    infix fun being(that: T): IsTemporallyFullyBounded<T>
    infix fun being(that: String): IsTemporallyFullyBounded<T>
}
