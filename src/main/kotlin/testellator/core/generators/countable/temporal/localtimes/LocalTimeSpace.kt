package testellator.core.generators.countable.temporal.localtimes

import testellator.core.spaces.StringRepresentableSpace
import testellator.extension.absDiff
import testellator.extension.minus
import testellator.extension.plus
import java.time.LocalTime

object LocalTimeSpace : StringRepresentableSpace<LocalTime> {
    override fun fullRange(): ClosedRange<LocalTime> = LocalTime.MIN..LocalTime.MAX
    override fun intersect(leftOperand: ClosedRange<LocalTime>, rightOperand: ClosedRange<LocalTime>): Boolean =
        leftOperand.start <= rightOperand.endInclusive && leftOperand.endInclusive >= rightOperand.start

    override fun measure(range: ClosedRange<LocalTime>): ULong =
        1uL + range.endInclusive.toNanoOfDay() absDiff range.start.toNanoOfDay()

    override fun parse(alternativeRepresentation: String): LocalTime = LocalTime.parse(alternativeRepresentation)

    override fun range(inclusiveLowerBound: LocalTime, inclusiveUpperBound: LocalTime): ClosedRange<LocalTime> =
        inclusiveLowerBound..inclusiveUpperBound

    override fun shiftLeft(leftOperand: LocalTime, rightOperand: ULong): LocalTime =
        LocalTime.ofNanoOfDay(leftOperand.toNanoOfDay() - rightOperand)

    override fun shiftRight(leftOperand: LocalTime, rightOperand: ULong): LocalTime =
        LocalTime.ofNanoOfDay(leftOperand.toNanoOfDay() + rightOperand)
}
