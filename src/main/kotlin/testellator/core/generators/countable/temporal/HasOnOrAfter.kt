package testellator.core.generators.countable.temporal

interface HasOnOrAfter<T> {
    infix fun onOrAfter(that: T): IsTemporallyLowerBounded<T>
    infix fun onOrAfter(that: String): IsTemporallyLowerBounded<T>
}
