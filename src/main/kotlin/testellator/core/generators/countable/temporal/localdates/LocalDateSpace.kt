package testellator.core.generators.countable.temporal.localdates

import testellator.core.spaces.StringRepresentableSpace
import testellator.extension.absDiff
import testellator.extension.minus
import testellator.extension.plus
import java.time.LocalDate

object LocalDateSpace : StringRepresentableSpace<LocalDate> {
    override fun fullRange(): ClosedRange<LocalDate> = LocalDate.MIN..LocalDate.MAX
    override fun intersect(leftOperand: ClosedRange<LocalDate>, rightOperand: ClosedRange<LocalDate>): Boolean =
        leftOperand.start <= rightOperand.endInclusive && leftOperand.endInclusive >= rightOperand.start

    override fun measure(range: ClosedRange<LocalDate>): ULong =
        1uL + range.endInclusive.toEpochDay() absDiff range.start.toEpochDay()

    override fun parse(alternativeRepresentation: String): LocalDate = LocalDate.parse(alternativeRepresentation)

    override fun range(inclusiveLowerBound: LocalDate, inclusiveUpperBound: LocalDate): ClosedRange<LocalDate> =
        inclusiveLowerBound..inclusiveUpperBound

    override fun shiftLeft(leftOperand: LocalDate, rightOperand: ULong): LocalDate =
        LocalDate.ofEpochDay(leftOperand.toEpochDay() - rightOperand)

    override fun shiftRight(leftOperand: LocalDate, rightOperand: ULong): LocalDate =
        LocalDate.ofEpochDay(leftOperand.toEpochDay() + rightOperand)
}
