package testellator.core.generators.countable.temporal

interface HasAndBefore<T> {
    infix fun andBefore(that: T): IsTemporallyFullyBounded<T>
    infix fun andBefore(that: String): IsTemporallyFullyBounded<T>
}
