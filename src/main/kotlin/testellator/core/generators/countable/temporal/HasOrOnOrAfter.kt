package testellator.core.generators.countable.temporal

interface HasOrOnOrAfter<T> {
    infix fun orOnOrAfter(that: T): IsTemporallyLowerBounded<T>
    infix fun orOnOrAfter(that: String): IsTemporallyLowerBounded<T>
}
