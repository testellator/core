package testellator.core.generators.countable.temporal.localtimes

import testellator.core.generators.countable.CountableGenerator
import testellator.core.generators.countable.numeric.IsTemporallyGenerating
import testellator.core.generators.countable.temporal.FullyBoundedTemporal
import testellator.core.generators.countable.temporal.IsTemporallyFullyBounded
import testellator.core.generators.countable.temporal.IsTemporallyLowerBounded
import testellator.core.generators.countable.temporal.LowerBoundedTemporal
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.Generator
import java.time.LocalTime

class GeneratedLocalTimes : IsTemporallyGenerating<LocalTime> {
    override fun any(): Generator<LocalTime> = CountableGenerator(LocalTimeSpace)
    override infix fun being(that: LocalTime): IsTemporallyFullyBounded<LocalTime> =
        FullyBoundedTemporal(LocalTimeSpace, that)

    override infix fun being(that: String): IsTemporallyFullyBounded<LocalTime> =
        FullyBoundedTemporal(LocalTimeSpace, LocalTimeSpace.parse(that))

    override infix fun onOrAfter(that: LocalTime): IsTemporallyLowerBounded<LocalTime> =
        LowerBoundedTemporal(LocalTimeSpace, that, Inclusive)

    override infix fun onOrAfter(that: String): IsTemporallyLowerBounded<LocalTime> =
        LowerBoundedTemporal(LocalTimeSpace, LocalTimeSpace.parse(that), Inclusive)

    override infix fun after(that: LocalTime): IsTemporallyLowerBounded<LocalTime> =
        LowerBoundedTemporal(LocalTimeSpace, that, Exclusive)

    override infix fun after(that: String): IsTemporallyLowerBounded<LocalTime> =
        LowerBoundedTemporal(LocalTimeSpace, LocalTimeSpace.parse(that), Exclusive)
}
