package testellator.core.generators.countable.temporal

interface HasAfter<T> {
    infix fun after(that: T): IsTemporallyLowerBounded<T>
    infix fun after(that: String): IsTemporallyLowerBounded<T>
}
