package testellator.core.generators.countable.temporal.localdatetimes

import testellator.core.generators.countable.CountableGenerator
import testellator.core.generators.countable.numeric.IsTemporallyGenerating
import testellator.core.generators.countable.temporal.FullyBoundedTemporal
import testellator.core.generators.countable.temporal.IsTemporallyFullyBounded
import testellator.core.generators.countable.temporal.IsTemporallyLowerBounded
import testellator.core.generators.countable.temporal.LowerBoundedTemporal
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.Generator
import java.time.LocalDateTime

class GeneratedLocalDateTimes : IsTemporallyGenerating<LocalDateTime> {

    override fun any(): Generator<LocalDateTime> = CountableGenerator(LocalDateTimeSpace)
    override infix fun being(that: LocalDateTime): IsTemporallyFullyBounded<LocalDateTime> =
        FullyBoundedTemporal(LocalDateTimeSpace, that)

    override infix fun being(that: String): IsTemporallyFullyBounded<LocalDateTime> =
        FullyBoundedTemporal(LocalDateTimeSpace, LocalDateTimeSpace.parse(that))

    override infix fun onOrAfter(that: LocalDateTime): IsTemporallyLowerBounded<LocalDateTime> =
        LowerBoundedTemporal(LocalDateTimeSpace, that, Inclusive)

    override infix fun onOrAfter(that: String): IsTemporallyLowerBounded<LocalDateTime> =
        LowerBoundedTemporal(LocalDateTimeSpace, LocalDateTimeSpace.parse(that), Inclusive)

    override infix fun after(that: LocalDateTime): IsTemporallyLowerBounded<LocalDateTime> =
        LowerBoundedTemporal(LocalDateTimeSpace, that, Exclusive)

    override infix fun after(that: String): IsTemporallyLowerBounded<LocalDateTime> =
        LowerBoundedTemporal(LocalDateTimeSpace, LocalDateTimeSpace.parse(that), Exclusive)
}
