package testellator.core.generators.countable.temporal

interface HasOrBeing<T> {
    infix fun orBeing(that: T): IsTemporallyFullyBounded<T>
    infix fun orBeing(that: String): IsTemporallyFullyBounded<T>
}
