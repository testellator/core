package testellator.core.generators.countable.temporal

interface HasOrAfter<T> {
    infix fun orAfter(that: T): IsTemporallyLowerBounded<T>
    infix fun orAfter(that: String): IsTemporallyLowerBounded<T>
}
