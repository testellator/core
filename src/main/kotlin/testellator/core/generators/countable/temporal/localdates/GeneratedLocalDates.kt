package testellator.core.generators.countable.temporal.localdates

import testellator.core.generators.countable.CountableGenerator
import testellator.core.generators.countable.numeric.IsTemporallyGenerating
import testellator.core.generators.countable.temporal.FullyBoundedTemporal
import testellator.core.generators.countable.temporal.IsTemporallyFullyBounded
import testellator.core.generators.countable.temporal.IsTemporallyLowerBounded
import testellator.core.generators.countable.temporal.LowerBoundedTemporal
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.Generator
import java.time.LocalDate

class GeneratedLocalDates : IsTemporallyGenerating<LocalDate> {
    override fun any(): Generator<LocalDate> = CountableGenerator(LocalDateSpace)
    override infix fun being(that: LocalDate): IsTemporallyFullyBounded<LocalDate> =
        FullyBoundedTemporal(LocalDateSpace, that)

    override infix fun being(that: String): IsTemporallyFullyBounded<LocalDate> =
        FullyBoundedTemporal(LocalDateSpace, LocalDateSpace.parse(that))

    override infix fun onOrAfter(that: LocalDate): IsTemporallyLowerBounded<LocalDate> =
        LowerBoundedTemporal(LocalDateSpace, that, Inclusive)

    override infix fun onOrAfter(that: String): IsTemporallyLowerBounded<LocalDate> =
        LowerBoundedTemporal(LocalDateSpace, LocalDateSpace.parse(that), Inclusive)

    override infix fun after(that: LocalDate): IsTemporallyLowerBounded<LocalDate> =
        LowerBoundedTemporal(LocalDateSpace, that, Exclusive)

    override infix fun after(that: String): IsTemporallyLowerBounded<LocalDate> =
        LowerBoundedTemporal(LocalDateSpace, LocalDateSpace.parse(that), Exclusive)
}
