package testellator.core.generators.countable.temporal

import testellator.core.generators.Generator

interface IsTemporallyFullyBounded<T> : Generator<T>, HasOrBeing<T>, HasOrOnOrAfter<T>, HasOrAfter<T>
