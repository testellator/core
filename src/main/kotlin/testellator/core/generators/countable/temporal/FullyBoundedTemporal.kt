package testellator.core.generators.countable.temporal

import testellator.core.generators.countable.CountableGenerator
import testellator.core.spaces.StringRepresentableSpace
import testellator.core.generators.BoundType
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive

class FullyBoundedTemporal<T : Comparable<T>> internal constructor(
    private val space: StringRepresentableSpace<T>,
    private val generator: CountableGenerator<T>,
    lowerBound: T,
    lowerBoundType: BoundType,
    upperBound: T,
    upperBoundType: BoundType,
    checkBrevity: Boolean = true
) : IsTemporallyFullyBounded<T> {

    internal constructor(space: StringRepresentableSpace<T>, value: T) : this(
        space,
        CountableGenerator(space),
        value
    )

    internal constructor(
        space: StringRepresentableSpace<T>,
        generator: CountableGenerator<T>,
        value: T
    ) : this(
        space,
        generator,
        value,
        Inclusive,
        value,
        Inclusive,
        false
    )

    init {
        generator.addRange(lowerBound, lowerBoundType, upperBound, upperBoundType, checkBrevity)
    }

    override fun orAfter(that: T): IsTemporallyLowerBounded<T> = LowerBoundedTemporal(space, generator, that, Exclusive)

    override fun orAfter(that: String): IsTemporallyLowerBounded<T> =
        LowerBoundedTemporal(space, generator, space.parse(that), Exclusive)

    override fun orBeing(that: T): IsTemporallyFullyBounded<T> = FullyBoundedTemporal(space, generator, that)

    override fun orBeing(that: String): IsTemporallyFullyBounded<T> =
        FullyBoundedTemporal(space, generator, space.parse(that))

    override fun next(): T = generator.next()

    override fun orOnOrAfter(that: T): IsTemporallyLowerBounded<T> =
        LowerBoundedTemporal(space, generator, that, Inclusive)

    override fun orOnOrAfter(that: String): IsTemporallyLowerBounded<T> =
        LowerBoundedTemporal(space, generator, space.parse(that), Inclusive)
}
