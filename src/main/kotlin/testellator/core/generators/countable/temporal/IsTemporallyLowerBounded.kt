package testellator.core.generators.countable.temporal

interface IsTemporallyLowerBounded<T> : HasAndOnOrBefore<T>, HasAndBefore<T>
