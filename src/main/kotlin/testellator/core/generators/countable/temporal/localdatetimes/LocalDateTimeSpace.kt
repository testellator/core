package testellator.core.generators.countable.temporal.localdatetimes

import testellator.core.spaces.StringRepresentableSpace
import testellator.extension.absDiff
import testellator.extension.minus
import testellator.extension.plus
import java.time.LocalDateTime
import java.time.ZoneOffset.UTC

object LocalDateTimeSpace : StringRepresentableSpace<LocalDateTime> {
    override fun fullRange(): ClosedRange<LocalDateTime> = LocalDateTime.MIN..LocalDateTime.MAX
    override fun intersect(leftOperand: ClosedRange<LocalDateTime>, rightOperand: ClosedRange<LocalDateTime>): Boolean =
        leftOperand.start <= rightOperand.endInclusive && leftOperand.endInclusive >= rightOperand.start

    override fun measure(range: ClosedRange<LocalDateTime>): ULong =
        1uL + range.endInclusive.toEpochSecond(UTC) absDiff range.start.toEpochSecond(UTC)

    override fun parse(alternativeRepresentation: String): LocalDateTime =
        LocalDateTime.parse(alternativeRepresentation)

    override fun range(
        inclusiveLowerBound: LocalDateTime,
        inclusiveUpperBound: LocalDateTime
    ): ClosedRange<LocalDateTime> =
        inclusiveLowerBound..inclusiveUpperBound

    override fun shiftLeft(leftOperand: LocalDateTime, rightOperand: ULong): LocalDateTime =
        LocalDateTime.ofEpochSecond(leftOperand.toEpochSecond(UTC) - rightOperand, 0, UTC)

    override fun shiftRight(leftOperand: LocalDateTime, rightOperand: ULong): LocalDateTime =
        LocalDateTime.ofEpochSecond(leftOperand.toEpochSecond(UTC) + rightOperand, 0, UTC)
}
