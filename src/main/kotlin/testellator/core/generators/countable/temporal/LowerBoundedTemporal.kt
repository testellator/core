package testellator.core.generators.countable.temporal

import testellator.core.generators.countable.CountableGenerator
import testellator.core.spaces.StringRepresentableSpace
import testellator.core.generators.BoundType
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive

class LowerBoundedTemporal<T : Comparable<T>> internal constructor(
    private val space: StringRepresentableSpace<T>,
    private val generator: CountableGenerator<T>,
    private val lowerBound: T,
    private val lowerBoundType: BoundType
) : IsTemporallyLowerBounded<T> {

    internal constructor(space: StringRepresentableSpace<T>, lowerBound: T, lowerBoundType: BoundType) : this(
        space,
        CountableGenerator(space),
        lowerBound,
        lowerBoundType
    )

    override fun andBefore(that: T): IsTemporallyFullyBounded<T> =
        FullyBoundedTemporal(space, generator, lowerBound, lowerBoundType, that, Exclusive)

    override fun andBefore(that: String): IsTemporallyFullyBounded<T> =
        FullyBoundedTemporal(space, generator, lowerBound, lowerBoundType, space.parse(that), Exclusive)

    override fun andOnOrBefore(that: T): IsTemporallyFullyBounded<T> =
        FullyBoundedTemporal(space, generator, lowerBound, lowerBoundType, that, Inclusive)

    override fun andOnOrBefore(that: String): IsTemporallyFullyBounded<T> =
        FullyBoundedTemporal(space, generator, lowerBound, lowerBoundType, space.parse(that), Inclusive)
}
