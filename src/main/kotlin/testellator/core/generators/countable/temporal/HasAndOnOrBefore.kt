package testellator.core.generators.countable.temporal

interface HasAndOnOrBefore<T> {
    infix fun andOnOrBefore(that: T): IsTemporallyFullyBounded<T>
    infix fun andOnOrBefore(that: String): IsTemporallyFullyBounded<T>
}
