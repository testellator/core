package testellator.core.generators.countable

import testellator.core.generators.Generator

interface HasAny<T> {
    fun any(): Generator<T>
}
