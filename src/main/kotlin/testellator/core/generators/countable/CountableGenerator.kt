package testellator.core.generators.countable

import testellator.core.generators.BoundType
import testellator.core.generators.Generator
import testellator.core.generators.exceptions.EqualToIsMoreSuccinctException
import testellator.core.generators.exceptions.LowerBoundIsGreaterThanUpperBoundException
import testellator.core.generators.exceptions.RangeIntersectsWithPreviouslyAddedException
import testellator.core.spaces.Space
import testellator.extension.indexIntoRanges
import java.util.*
import kotlin.random.Random
import kotlin.random.nextULong

class CountableGenerator<T : Comparable<T>>(private val space: Space<T>) : Generator<T> {

    private val ranges: MutableList<ClosedRange<T>> = LinkedList()
    private var valueCount: ULong = 0uL

    override fun next(): T {
        return if (ranges.isEmpty()) {
            val range = space.fullRange()
            val index: ULong = if (space.measure(range) == 0uL) {
                Random.nextULong()
            } else {
                Random.nextULong(0uL, space.measure(range))
            }
            space.shiftRight(range.start, index)
        } else {
            val index = Random.nextULong(0uL, valueCount)
            space.indexIntoRanges(ranges, index)
        }
    }

    internal fun addRange(
        lowerBound: T,
        lowerBoundType: BoundType,
        upperBound: T,
        upperBoundType: BoundType,
        checkBrevity: Boolean
    ) {
        val inclusiveLowerBound = space.shiftRight(lowerBound, lowerBoundType.offset)
        val inclusiveUpperBound = space.shiftLeft(upperBound, upperBoundType.offset)

        if (inclusiveLowerBound > inclusiveUpperBound)
            throw LowerBoundIsGreaterThanUpperBoundException(
                lowerBound,
                lowerBoundType,
                inclusiveLowerBound,
                upperBound,
                upperBoundType,
                inclusiveUpperBound
            )

        if (checkBrevity && inclusiveLowerBound == inclusiveUpperBound)
            throw EqualToIsMoreSuccinctException(
                lowerBound,
                lowerBoundType,
                inclusiveLowerBound,
                upperBound,
                upperBoundType,
                inclusiveUpperBound
            )

        val range = space.range(inclusiveLowerBound, inclusiveUpperBound)
        val intersectingRanges = intersectingRanges(range)
        if (intersectingRanges.isNotEmpty())
            throw RangeIntersectsWithPreviouslyAddedException(range, intersectingRanges)

        addRange(range)
    }

    private fun intersectingRanges(range: ClosedRange<T>): Collection<ClosedRange<T>> =
        ranges.filter { space.intersect(it, range) }

    private fun addRange(range: ClosedRange<T>) {
        ranges.add(range)
        valueCount += space.measure(range)
    }
}
