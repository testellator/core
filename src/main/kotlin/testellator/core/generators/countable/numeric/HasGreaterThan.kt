package testellator.core.generators.countable.numeric

interface HasGreaterThan<T> {
    infix fun greaterThan(that: T): IsNumericallyLowerBounded<T>
}
