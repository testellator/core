package testellator.core.generators.countable.numeric

import testellator.core.generators.Generator

interface IsNumericallyFullyBounded<T> : Generator<T>, HasOrEqualTo<T>, HasOrStrictlyGreaterThan<T>, HasOrGreaterThanOrEqualTo<T>
