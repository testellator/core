package testellator.core.generators.countable.numeric

interface HasOrStrictlyGreaterThan<T> {
    infix fun orStrictlyGreaterThan(that: T): IsNumericallyLowerBounded<T>
}
