package testellator.core.generators.countable.numeric

interface HasAndStrictlyLessThan<T> {
    infix fun andStrictlyLessThan(that: T): IsNumericallyFullyBounded<T>
}
