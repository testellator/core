package testellator.core.generators.countable.numeric.chars

import testellator.core.generators.countable.CountableGenerator
import testellator.core.generators.countable.numeric.*
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.Generator

class GeneratedChars : IsNumericallyGenerating<Char> {
    override fun any(): Generator<Char> = CountableGenerator(CharSpace)
    override infix fun equalTo(that: Char): IsNumericallyFullyBounded<Char> = FullyBoundedNumeric(CharSpace, that)
    override infix fun greaterThanOrEqualTo(that: Char): IsNumericallyLowerBounded<Char> =
        LowerBoundedNumeric(CharSpace, that, Inclusive)

    override infix fun greaterThan(that: Char): IsNumericallyLowerBounded<Char> = LowerBoundedNumeric(CharSpace, that, Exclusive)
}
