package testellator.core.generators.countable.numeric

interface IsNumericallyLowerBounded<T> : HasAndStrictlyLessThan<T>, HasAndLessThanOrEqualTo<T>
