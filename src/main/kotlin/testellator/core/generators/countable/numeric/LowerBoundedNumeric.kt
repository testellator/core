package testellator.core.generators.countable.numeric

import testellator.core.generators.BoundType
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.countable.CountableGenerator
import testellator.core.spaces.Space

class LowerBoundedNumeric<T : Comparable<T>> internal constructor(
    private val space: Space<T>,
    private val generator: CountableGenerator<T>,
    private val lowerBound: T,
    private val lowerBoundType: BoundType
) : IsNumericallyLowerBounded<T> {

    internal constructor(space: Space<T>, lowerBound: T, lowerBoundType: BoundType) : this(
        space,
        CountableGenerator(space),
        lowerBound,
        lowerBoundType
    )

    override fun andLessThanOrEqualTo(that: T): IsNumericallyFullyBounded<T> =
        FullyBoundedNumeric(space, generator, lowerBound, lowerBoundType, that, Inclusive)

    override fun andStrictlyLessThan(that: T): IsNumericallyFullyBounded<T> =
        FullyBoundedNumeric(space, generator, lowerBound, lowerBoundType, that, Exclusive)
}
