package testellator.core.generators.countable.numeric

interface HasEqualTo<T> {
    infix fun equalTo(that: T): IsNumericallyFullyBounded<T>
}
