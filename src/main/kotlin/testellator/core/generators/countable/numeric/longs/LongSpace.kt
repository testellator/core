package testellator.core.generators.countable.numeric.longs

import testellator.core.spaces.Space
import testellator.extension.absDiff
import testellator.extension.minus
import testellator.extension.plus

object LongSpace : Space<Long> {
    override fun fullRange(): ClosedRange<Long> = Long.MIN_VALUE..Long.MAX_VALUE
    override fun intersect(leftOperand: ClosedRange<Long>, rightOperand: ClosedRange<Long>): Boolean =
        leftOperand.start <= rightOperand.endInclusive && leftOperand.endInclusive >= rightOperand.start

    override fun measure(range: ClosedRange<Long>): ULong = (range.endInclusive absDiff range.start) + 1uL

    override fun range(inclusiveLowerBound: Long, inclusiveUpperBound: Long): ClosedRange<Long> =
        inclusiveLowerBound..inclusiveUpperBound

    override fun shiftLeft(leftOperand: Long, rightOperand: ULong): Long = leftOperand - rightOperand

    override fun shiftRight(leftOperand: Long, rightOperand: ULong): Long = leftOperand + rightOperand
}
