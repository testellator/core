package testellator.core.generators.countable.numeric

import testellator.core.generators.Generator

interface GeneratedNumeric<T: Comparable<T>> {
    fun any(): Generator<T>
}
