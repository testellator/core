package testellator.core.generators.countable.numeric

import testellator.core.generators.countable.HasAny
import testellator.core.generators.countable.temporal.HasAfter
import testellator.core.generators.countable.temporal.HasBeing
import testellator.core.generators.countable.temporal.HasOnOrAfter

interface IsTemporallyGenerating<T> : HasAfter<T>, HasAny<T>, HasBeing<T>, HasOnOrAfter<T>
