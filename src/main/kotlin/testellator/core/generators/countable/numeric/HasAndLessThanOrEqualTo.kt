package testellator.core.generators.countable.numeric

interface HasAndLessThanOrEqualTo<T> {
    infix fun andLessThanOrEqualTo(that: T): IsNumericallyFullyBounded<T>
}
