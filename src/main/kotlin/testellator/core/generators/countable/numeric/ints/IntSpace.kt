package testellator.core.generators.countable.numeric.ints

import testellator.core.spaces.Space
import testellator.extension.minus
import testellator.extension.plus

object IntSpace : Space<Int> {
    override fun fullRange(): ClosedRange<Int> = Int.MIN_VALUE..Int.MAX_VALUE
    override fun intersect(leftOperand: ClosedRange<Int>, rightOperand: ClosedRange<Int>): Boolean =
        leftOperand.start <= rightOperand.endInclusive && leftOperand.endInclusive >= rightOperand.start

    override fun measure(range: ClosedRange<Int>): ULong =
        (range.endInclusive.toLong() - range.start.toLong()).toULong() + 1uL

    override fun range(inclusiveLowerBound: Int, inclusiveUpperBound: Int): ClosedRange<Int> =
        inclusiveLowerBound..inclusiveUpperBound

    override fun shiftLeft(leftOperand: Int, rightOperand: ULong): Int =
        (leftOperand.toLong() - rightOperand).toInt()

    override fun shiftRight(leftOperand: Int, rightOperand: ULong): Int =
        (leftOperand.toLong() + rightOperand).toInt()
}
