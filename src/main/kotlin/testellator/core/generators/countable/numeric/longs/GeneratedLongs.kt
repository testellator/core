package testellator.core.generators.countable.numeric.longs

import testellator.core.generators.countable.CountableGenerator
import testellator.core.generators.countable.numeric.*
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.Generator

class GeneratedLongs : IsNumericallyGenerating<Long> {
    override fun any(): Generator<Long> = CountableGenerator(LongSpace)
    override infix fun equalTo(that: Long): IsNumericallyFullyBounded<Long> = FullyBoundedNumeric(LongSpace, that)
    override infix fun greaterThanOrEqualTo(that: Long): IsNumericallyLowerBounded<Long> =
        LowerBoundedNumeric(LongSpace, that, Inclusive)
    override infix fun greaterThan(that: Long): IsNumericallyLowerBounded<Long> = LowerBoundedNumeric(LongSpace, that, Exclusive)
}
