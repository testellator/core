package testellator.core.generators.countable.numeric

import testellator.core.generators.countable.HasAny

interface IsNumericallyGenerating<T> : HasAny<T>, HasEqualTo<T>, HasGreaterThan<T>, HasGreaterThanOrEqualTo<T>
