package testellator.core.generators.countable.numeric

import testellator.core.generators.BoundType
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.countable.CountableGenerator
import testellator.core.spaces.Space

class FullyBoundedNumeric<T : Comparable<T>> internal constructor(
    private val space: Space<T>,
    private val generator: CountableGenerator<T>,
    lowerBound: T,
    lowerBoundType: BoundType,
    upperBound: T,
    upperBoundType: BoundType,
    checkBrevity: Boolean = true
) : IsNumericallyFullyBounded<T> {

    internal constructor(space: Space<T>, value: T) : this(space, CountableGenerator(space), value)

    internal constructor(space: Space<T>, generator: CountableGenerator<T>, value: T) : this(
        space,
        generator,
        value,
        Inclusive,
        value,
        Inclusive,
        false
    )

    init {
        generator.addRange(lowerBound, lowerBoundType, upperBound, upperBoundType, checkBrevity)
    }

    override fun orEqualTo(that: T): IsNumericallyFullyBounded<T> = FullyBoundedNumeric(space, generator, that)

    override fun orGreaterThatOrEqualTo(that: T): IsNumericallyLowerBounded<T> =
        LowerBoundedNumeric(space, generator, that, Inclusive)

    override fun next(): T = generator.next()

    override fun orStrictlyGreaterThan(that: T): IsNumericallyLowerBounded<T> =
        LowerBoundedNumeric(space, generator, that, Exclusive)
}
