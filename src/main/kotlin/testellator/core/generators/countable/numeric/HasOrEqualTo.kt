package testellator.core.generators.countable.numeric

interface HasOrEqualTo<T> {
    infix fun orEqualTo(that: T): IsNumericallyFullyBounded<T>
}
