package testellator.core.generators.countable.numeric

interface HasGreaterThanOrEqualTo<T> {
    infix fun greaterThanOrEqualTo(that: T): IsNumericallyLowerBounded<T>
}
