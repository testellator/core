package testellator.core.generators.countable.numeric.ints

import testellator.core.generators.countable.CountableGenerator
import testellator.core.generators.countable.numeric.*
import testellator.core.generators.BoundType.Exclusive
import testellator.core.generators.BoundType.Inclusive
import testellator.core.generators.Generator

class GeneratedInts : IsNumericallyGenerating<Int> {
    override fun any(): Generator<Int> = CountableGenerator(IntSpace)
    override infix fun equalTo(that: Int): IsNumericallyFullyBounded<Int> = FullyBoundedNumeric(IntSpace, that)
    override infix fun greaterThanOrEqualTo(that: Int): IsNumericallyLowerBounded<Int> =
        LowerBoundedNumeric(IntSpace, that, Inclusive)

    override infix fun greaterThan(that: Int): IsNumericallyLowerBounded<Int> =
        LowerBoundedNumeric(IntSpace, that, Exclusive)
}
