package testellator.core.generators.countable.numeric

interface HasOrGreaterThanOrEqualTo<T> {
    infix fun orGreaterThatOrEqualTo(that: T): IsNumericallyLowerBounded<T>
}
