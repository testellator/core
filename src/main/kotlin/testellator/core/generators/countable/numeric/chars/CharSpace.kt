package testellator.core.generators.countable.numeric.chars

import testellator.core.spaces.Space
import testellator.extension.minus
import testellator.extension.plus

object CharSpace : Space<Char> {
    override fun fullRange(): ClosedRange<Char> = Char.MIN_VALUE..Char.MAX_VALUE
    override fun intersect(leftOperand: ClosedRange<Char>, rightOperand: ClosedRange<Char>): Boolean =
        leftOperand.start <= rightOperand.endInclusive && leftOperand.endInclusive >= rightOperand.start

    override fun measure(range: ClosedRange<Char>): ULong =
        (range.endInclusive.toLong() - range.start.toLong()).toULong() + 1uL

    override fun range(inclusiveLowerBound: Char, inclusiveUpperBound: Char): ClosedRange<Char> =
        inclusiveLowerBound..inclusiveUpperBound

    override fun shiftLeft(leftOperand: Char, rightOperand: ULong): Char =
        (leftOperand.toLong() - rightOperand).toChar()

    override fun shiftRight(leftOperand: Char, rightOperand: ULong): Char =
        (leftOperand.toLong() + rightOperand).toChar()
}
