package testellator.core.generators

import testellator.core.generators.countable.numeric.IsNumericallyGenerating
import testellator.core.generators.countable.numeric.IsTemporallyGenerating
import testellator.core.generators.countable.numeric.chars.GeneratedChars
import testellator.core.generators.countable.numeric.ints.GeneratedInts
import testellator.core.generators.countable.numeric.longs.GeneratedLongs
import testellator.core.generators.countable.temporal.localdates.GeneratedLocalDates
import testellator.core.generators.countable.temporal.localdatetimes.GeneratedLocalDateTimes
import testellator.core.generators.countable.temporal.localtimes.GeneratedLocalTimes
import testellator.core.generators.strings.GeneratedStrings
import testellator.core.generators.strings.IsStringGenerating
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

object Generated {
    @JvmStatic
    val Chars: IsNumericallyGenerating<Char>
        @JvmName("Chars")
        get() = GeneratedChars()

    @JvmStatic
    val Ints: IsNumericallyGenerating<Int>
        @JvmName("Ints")
        get() = GeneratedInts()

    @JvmStatic
    val LocalDates: IsTemporallyGenerating<LocalDate>
        @JvmName("LocalDates")
        get() = GeneratedLocalDates()

    @JvmStatic
    val LocalDateTimes: IsTemporallyGenerating<LocalDateTime>
        @JvmName("LocalDateTimes")
        get() = GeneratedLocalDateTimes()

    @JvmStatic
    val LocalTimes: IsTemporallyGenerating<LocalTime>
        @JvmName("LocalTimes")
        get() = GeneratedLocalTimes()

    @JvmStatic
    val Longs: IsNumericallyGenerating<Long>
        @JvmName("Longs")
        get() = GeneratedLongs()

    @JvmStatic
    val Strings: IsStringGenerating<String>
        @JvmName("Strings")
        get() = GeneratedStrings()
}
