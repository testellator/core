package testellator.core.generators

/**
 * A generator of potentially infinite sequence of objects
 *
 * @param T The type of objects to generate
 */
interface Generator<T> {

    /**
     * @return The next object in the generated sequence
     */
    fun next(): T
}
