package testellator.core.utilityclasses

import testellator.extension.hasFlag
import java.lang.reflect.Modifier
import java.lang.reflect.Modifier.PRIVATE

object UtilityClasses {

    @JvmStatic
    fun <T> hasJustOneConstructorWhichIsPrivateAndHasNoArguments(theClass: Class<T>): Boolean {
        val constructors = theClass.declaredConstructors
        return when (constructors.size) {
            0 -> true
            1 -> {
                val constructor = constructors[0]
                return constructor.modifiers hasFlag PRIVATE && constructor.parameterCount == 0
            }
            else -> false
        }
    }
}
