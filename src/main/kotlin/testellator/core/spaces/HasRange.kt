package testellator.core.spaces

interface HasRange<T : Comparable<T>> {
    fun range(inclusiveLowerBound: T, inclusiveUpperBound: T): ClosedRange<T>
}
