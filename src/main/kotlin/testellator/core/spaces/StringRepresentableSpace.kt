package testellator.core.spaces

interface StringRepresentableSpace<T : Comparable<T>> : Space<T>,
    HasAlternativeRepresentation<T, String>
