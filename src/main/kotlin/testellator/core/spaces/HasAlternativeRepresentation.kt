package testellator.core.spaces

interface HasAlternativeRepresentation<T, R> {
    fun parse(alternativeRepresentation: R): T
}
