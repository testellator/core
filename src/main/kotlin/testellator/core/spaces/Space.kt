package testellator.core.spaces

interface Space<T : Comparable<T>> :
    HasFullRange<T>,
    HasIntersect<T>,
    HasLeftShift<T>,
    HasMeasure<T>,
    HasRange<T>,
    HasRightShift<T>,
    PartialSpace<T>
