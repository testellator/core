package testellator.core.spaces

interface HasLeftShift<T> {
    fun shiftLeft(leftOperand: T, rightOperand: ULong): T
}
