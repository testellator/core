package testellator.core.spaces

interface HasMeasure<T: Comparable<T>> {
    fun measure(range: ClosedRange<T>): ULong
}
