package testellator.core.spaces

interface PartialSpace<T : Comparable<T>> :
        HasIntersect<T>,
        HasMeasure<T>,
        HasRightShift<T>
