package testellator.core.spaces

interface HasIntersect<T: Comparable<T>> {
    fun intersect(leftOperand: ClosedRange<T>, rightOperand: ClosedRange<T>): Boolean
}
