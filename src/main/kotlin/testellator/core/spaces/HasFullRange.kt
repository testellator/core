package testellator.core.spaces

interface HasFullRange<T: Comparable<T>> {
    fun fullRange(): ClosedRange<T>
}
