package testellator.core.spaces

interface HasRightShift<T> {
    fun shiftRight(leftOperand: T, rightOperand: ULong): T
}
