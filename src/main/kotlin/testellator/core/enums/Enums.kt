package testellator.core.enums

/**
 * Various utilities for dealing with Enums
 */
object Enums {

    /**
     * Builds a string that is guaranteed not to be the name of any instance of the given Enum type. This is often
     * useful for verifying exception handling of code that makes use of valueOf()
     *
     * @param enumClass The enum class whose instance names you want to avoid
     * @return A string that is guaranteed not to be the name of any instance of the given Enum type
     */
    fun <T : Enum<T>> notNameOfA(enumClass: Class<T>): String {
        return enumClass.enumConstants.map(this::differentCharacterFromOrdinal).joinToString("")
    }

    // Returns a character that differs in the zero-based i-th position of the Enum instance name where i is its ordinal
    // See https://en.wikipedia.org/wiki/Cantor%27s_diagonal_argument for the general idea
    internal fun <T : Enum<T>> differentCharacterFromOrdinal(v: T): Char {
        return if (v.ordinal >= v.name.length || v.name[v.ordinal] != 'X') 'X' else 'Y'
    }
}
