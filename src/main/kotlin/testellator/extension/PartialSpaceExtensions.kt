package testellator.extension

import testellator.core.spaces.PartialSpace

fun <T : Comparable<T>> PartialSpace<T>.indexIntoRanges(ranges: Iterable<ClosedRange<T>>, index: ULong): T {
    var localIndex: ULong = index
    for (range in ranges) {
        val measure: ULong = measure(range)
        if (localIndex < measure) {
            return shiftRight(range.start, localIndex)
        } else {
            localIndex -= measure
        }
    }
    throw IndexOutOfBoundsException("Index [$index] is out of bounds")
}
