package testellator.extension

infix fun Int.hasFlag(flag: Int): Boolean = this and flag == flag

infix fun Int.doesNotHaveFlag(flag: Int): Boolean = this and flag == 0
