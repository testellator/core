package testellator.extension

val MAX_SIGNED_AS_UNSIGNED = 0x7FFF_FFFF_FFFF_FFFF_uL

operator fun Long.plus(that: ULong): Long {
    return if (that <= MAX_SIGNED_AS_UNSIGNED)
        this + that.toLong()
    else {
        val remainder: ULong = that % 2_uL
        val halfish: ULong = that shr 1
        // Parentheses are important. Each addition needs to be signed + unsigned
        return ((this + halfish) + halfish) + remainder
    }
}

operator fun Long.minus(that: ULong): Long {
    return if (that <= MAX_SIGNED_AS_UNSIGNED)
        this - that.toLong()
    else {
        val remainder: ULong = that % 2_uL
        val halfish: ULong = that shr 1
        // Parentheses are important. Each addition needs to be signed - unsigned
        return ((this - halfish) - halfish) - remainder
    }
}

infix fun Long.absDiff(that: Long): ULong {
    // TODO: This is not pretty. I'm sure there's some clever binary shorthand!
    return if (this <= that) {
        when {
            that < 0 -> (-this).toULong() - (-that).toULong()
            this > 0 -> that.toULong() - this.toULong()
            else -> (-this).toULong() + that.toULong()
        }
    } else {
        that absDiff this
    }
}

operator fun ULong.plus(that: Long): Long = that + this

//operator fun ULong.minus(that: Long): Long = that - this
