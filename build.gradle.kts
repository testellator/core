plugins {
    jacoco
    kotlin("jvm") version "1.4.10"
    `maven-publish`
    id("org.jetbrains.dokka") version "0.9.16"
    signing
}

group = "com.gitlab.testellator"
version = "0.0.2"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    implementation("org.junit.jupiter:junit-jupiter:5.4.2")
}

val sourcesJar = task<Jar>("sourcesJar") {
    archiveClassifier.set("sources")
    from(sourceSets["main"].allSource)
}

val dokkaJar = task<Jar>("dokkaJar") {
    archiveClassifier.set("javadoc")
    group = JavaBasePlugin.DOCUMENTATION_GROUP
}

tasks {
    test {
        useJUnitPlatform()
    }

    jacocoTestReport {
        reports {
            xml.isEnabled = false
            csv.isEnabled = false
            html.isEnabled = true
            html.destination = file("$buildDir/jacocoHtml")
        }
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs = listOf(
            "-Xuse-experimental=kotlin.ExperimentalUnsignedTypes",
            "-XXLanguage:+InlineClasses")
    }
}

publishing {
    publications {
        create<MavenPublication>("core") {

            artifactId = "core"

            from(components["java"])

            artifacts {
                artifact(sourcesJar)
                artifact(dokkaJar)
            }

            pom {

                name.set("Testellator Core")
                description.set("Useful testing capabilities")
                url.set("https://gitlab.com/testellator/core")

                licenses {
                    license {
                        name.set("Apache License 2.0")
                        url.set("https://gitlab.com/testellator/core/-/tree/master/LICENSE")
                    }
                }
                
                developers {
                    developer {
                        id.set("pdcooper80@gmail.com")
                        name.set("Paul Cooper")
                        email.set("pdcooper80@gmail.com")
                    }
                }

                scm {
                    connection.set("https://gitlab.com/testellator/core.git")
                    developerConnection.set("https://gitlab.com/testellator/core.git")
                    url.set("https://gitlab.com/testellator/core")
                }
            }
        }
    }

    repositories {
        maven {

            credentials {
                val ossrhUsername: String by project
                val ossrhPassword: String by project
                username = ossrhUsername
                password = ossrhPassword
            }

            val releasesRepoUrl = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2")
            val snapshotsRepoUrl = uri("https://oss.sonatype.org/content/repositories/snapshots")
            url = if (version.toString().endsWith("SNAPSHOT")) snapshotsRepoUrl else releasesRepoUrl
        }
    }
}

signing {
    sign(publishing.publications["core"])
}
